﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;

[ExecuteInEditMode]
public class CustomizeController : MonoBehaviour
{
    [Header("Sprite Parts")]
    public SpriteRenderer[] spritePart;

    [Header("Sprite Options")]
    public Sprite[] bodyOptions;
    //EYE
    public Sprite[] lefteyeOptions;
    public Sprite[] righteyeOptions;
    //Head
    public Sprite[] ahogeOptions;
    public Sprite[] backhairOptions;
    public Sprite[] bangOptions;
    public Sprite[] lefthairOptions;
    public Sprite[] righthairOptions;
    

    
    
    
    [Space]
    public int[] index;
    private int[] ex_index;

    [Space]
    [SerializeField] private bool isEdit = false;
    
    public void Customize(string part , int indexpart)
    {
        if(part == PunSkinSetting.PLAYER_BODY)
        {
            index[0] = indexpart;
        }
    }

    private void Update()
    {
        ex_index = new int[spritePart.Length];
        if (index.Length != ex_index.Length)
        {
            index = new int[spritePart.Length];
        }

        ApplyPart();
        
    }
    
    public void ApplyPart()
    {
        for (int i = 0; i < spritePart.Length; i++)
        {
            spritePart[i].sprite = ConvertIndexToSprite(i, index[i]);
        }
    }

    public Sprite ConvertIndexToSprite(int part, int index)
    {
        switch (part)
        {
            case 0: return bodyOptions[index]; break;
            case 1: return lefteyeOptions[index]; break;
            case 2: return righteyeOptions[index]; break;
            case 3: return ahogeOptions[index]; break;
            case 4: return backhairOptions[index]; break;
            case 5: return bangOptions[index]; break;
            case 6: return lefthairOptions[index]; break;
            case 7: return righthairOptions[index]; break;
         
          
        }
        

        return null;
    }
    
    private int ConvertIndexToOption(int part)
    {
        switch (part)
        {
            case 0: return bodyOptions.Length; break;
            case 1: return lefteyeOptions.Length; break;
            case 2: return righteyeOptions.Length; break;
            case 3: return ahogeOptions.Length; break;
            case 4: return backhairOptions.Length; break;
            case 5: return bangOptions.Length;break;
            case 6: return lefthairOptions.Length; break;
            case 7: return righthairOptions.Length;break;
           
        
        }

        return 0;
    }

   
    
    public void NextPart(int part)
    {
        isEdit = true;
        index[part]++;
        
        if (index[part] >= ConvertIndexToOption(part))
        {
            index[part] = 0;
        }
        
        ApplyPart();
    }
    
    public void PreviousPart(int part)
    {
        isEdit = true;
        index[part]--;
        
        if (index[part] < 0)
        {
            index[part] = ConvertIndexToOption(part) - 1;
        }
        //_partTemp = part;
        ApplyPart();
    }
    

    
    
    public void Save()
    {
        JSONObject playerAppearanceJson = new JSONObject();
        playerAppearanceJson.Add("Body",index[0]);
        playerAppearanceJson.Add("Lefteye",index[1]);
        playerAppearanceJson.Add("Righteye",index[2]);
        playerAppearanceJson.Add("Ahoge",index[3]);
        playerAppearanceJson.Add("Backhair",index[4]);
        playerAppearanceJson.Add("Bang",index[5]);
        playerAppearanceJson.Add("Lefthair",index[6]);
        playerAppearanceJson.Add("Righthair",index[7]);
        
       

        
        Debug.Log(playerAppearanceJson.ToString());
        //Save Json
        string path = Directory.GetCurrentDirectory() + "Assets/PlayerData/";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        File.WriteAllText(path + "PlayerAppearance.json", playerAppearanceJson.ToString());
    }

    public void Load()
    {
        isEdit = false;
        string path = Directory.GetCurrentDirectory() + "Assets/PlayerData/PlayerAppearance.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerAppearanceJson = (JSONObject)JSON.Parse(jsonString);
        spritePart[0].sprite = bodyOptions[playerAppearanceJson["Body"]]; index[0] = playerAppearanceJson["Body"];
        spritePart[1].sprite = lefteyeOptions[playerAppearanceJson["Lefteye"]]; index[1] = playerAppearanceJson["Lefteye"];
        spritePart[2].sprite = righteyeOptions[playerAppearanceJson["Righteye"]]; index[2] = playerAppearanceJson["Righteye"];
        spritePart[3].sprite=ahogeOptions[playerAppearanceJson["Ahoge"]];index[3]=playerAppearanceJson["Ahoge"];
        spritePart[4].sprite=backhairOptions[playerAppearanceJson["Backhair"]];index[4]=playerAppearanceJson["Backhair"];
        spritePart[5].sprite=backhairOptions[playerAppearanceJson["Bang"]];index[5]=playerAppearanceJson["Bang"];
        spritePart[6].sprite=backhairOptions[playerAppearanceJson["lefthair"]];index[6]=playerAppearanceJson["lefthair"];
        spritePart[7].sprite=backhairOptions[playerAppearanceJson["righthair"]];index[7]=playerAppearanceJson["righthair"];
        
    }
}
