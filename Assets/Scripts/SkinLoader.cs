﻿using System;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using SimpleJSON;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class SkinLoader : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{
    private Player _player;
    
    [Header("Sprite Parts")]
    public SpriteRenderer[] spritePart;

    [Header("Sprite Options")]
    public Sprite[] bodyOptions;
    public Sprite[] lefteyeOptions;
    public Sprite[] righteyeOptions;
    public Sprite[] ahogeOptions;
    public Sprite[] backhairOptions;

    public Sprite[] bangOptions;
    public Sprite[] lefthairOptions;
    public Sprite[] righthairOptions;
    
    
    
    public int[] index;

    [SerializeField] private bool newPlayerJoin = false;
    
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (photonView.IsMine)
        {
            GetSkinIndex();
        }
    }

    private void Start()
    {
        ChangeSkinProperties();
    }

   
    public void GetSkinIndex()
    {
        string path = Directory.GetCurrentDirectory() + "Assets/PlayerData/PlayerAppearance.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerAppearanceJson = (JSONObject)JSON.Parse(jsonString);
        index[0] = playerAppearanceJson["Body"];
        index[1] = playerAppearanceJson["Lefteye"];
        index[2] = playerAppearanceJson["Righteye"];
        index[3] = playerAppearanceJson["Ahoge"];
        index[4] =playerAppearanceJson["Backhair"];
        index[5]=playerAppearanceJson["Bang"];
        index[6] =playerAppearanceJson["Lefthair"];
        index[7]=playerAppearanceJson["Righthair"];
    
      
        
        
        Debug.Log("Body: " + index[0] + "/n" 
        + "lefteye: " + index[1] + "/n"
        + "righteye: " + index[2] + "/n"
        
        );
    }

    private void ChangeSkinProperties()
    {
        if (photonView.IsMine)
        {
            Hashtable skins = new Hashtable
            {
                {PunSkinSetting.PLAYER_BODY, index[0]},
                {PunSkinSetting.PLAYER_LEFTEYE, index[1]},
                {PunSkinSetting.PLAYER_RIGHTEYE, index[2]},
                {PunSkinSetting.PLAYER_AHOGE,index[3]},
                {PunSkinSetting.PLAYER_BACKHAIR,index[4]},
                {PunSkinSetting.PLAYER_BANG,index[5]},
                {PunSkinSetting.PLAYER_LEFTHAIR,index[6]},
                {PunSkinSetting.PLAYER_RIGHTHAIR,index[7]},

                
                
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(skins);
            for (int i = 0; i < spritePart.Length; i++)
            {
                spritePart[i].sprite = ConvertIndexToSprite(i, index[i]);
            }
        }
        else
        {
            OnPlayerPropertiesUpdate(photonView.Owner, photonView.Owner.CustomProperties);
            //_player = photonView.Owner;
            //spritePart[0].sprite = bodyOptions[(int)_player.CustomProperties[PunSkinSetting.PLAYER_BODY]];
            //Debug.Log(_player.CustomProperties[PunSkinSetting.PLAYER_BODY].ToString());
        }
    }
    
    public Sprite ConvertIndexToSprite(int part, int index)
    {
        switch (part)
        {
            case 0: return bodyOptions[index]; break;
            case 1: return lefteyeOptions[index]; break;
            case 2: return righteyeOptions[index]; break;
            case 3: return ahogeOptions[index]; break;
            case 4: return backhairOptions[index]; break;
            case 5: return bangOptions[index]; break;
            case 6: return lefthairOptions[index]; break;
            case 7: return righthairOptions[index]; break;
            
        }
        //Debug.Log("Convert: " + index);

        return null;
    }
    Sprite ConvertPropsToSprite(int part, Hashtable props)
    {
        switch (part)
        {
            case 0: return bodyOptions[(int) props[PunSkinSetting.PLAYER_BODY]]; break;
            case 1: return lefteyeOptions[(int) props[PunSkinSetting.PLAYER_LEFTEYE]]; break;
            case 2: return righteyeOptions[(int) props[PunSkinSetting.PLAYER_RIGHTEYE]]; break;
            case 3: return ahogeOptions[(int)props[PunSkinSetting.PLAYER_AHOGE]]; break;
            case 4: return backhairOptions[(int)props[PunSkinSetting.PLAYER_BACKHAIR]];break;
            case 5: return bangOptions[(int)props[PunSkinSetting.PLAYER_BANG]];break;
            case 6: return lefthairOptions[(int)props[PunSkinSetting.PLAYER_LEFTHAIR]];break;
            case 7: return righthairOptions[(int)props[PunSkinSetting.PLAYER_RIGHTHAIR]];break;
           
        }
        return null;
    }
    
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (targetPlayer.ActorNumber == photonView.ControllerActorNr)
        {
            for (int i = 0; i < spritePart.Length; i++)
            {
                spritePart[i].sprite = ConvertPropsToSprite(i, changedProps);
            }
        }
        return;
    }
}
