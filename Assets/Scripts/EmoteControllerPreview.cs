﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmoteControllerPreview : MonoBehaviour
{
    private float waitTime=5f;
    private bool canEmotes=true;
    [SerializeField] GameObject Emote_Angly;
    [SerializeField] GameObject Emote_Happy;

    [SerializeField] GameObject Emote_Creepy;
    [SerializeField] GameObject Emote_Shy;

    private void Update()
    {
        //Angly
        if(Input.GetKey(KeyCode.Keypad0)&&canEmotes==true)
        {
            Emote_Angly.SetActive(true);
            canEmotes=false;
        }
        if(Emote_Angly.activeInHierarchy==true)
        {
            TimeUpdate();
            

        }
         //Happy
        if(Input.GetKey(KeyCode.Keypad1)&&canEmotes==true)
        {
            Emote_Happy.SetActive(true);
            canEmotes=false;
        }
        if(Emote_Happy.activeInHierarchy==true){
            TimeUpdate();
           
        }
        //Creepy
        if(Input.GetKey(KeyCode.Keypad2)&&canEmotes==true)
        {
            Emote_Creepy.SetActive(true);
            canEmotes=false;
        }
        if(Emote_Creepy.activeInHierarchy==true){
            TimeUpdate();
           
        }
        //Shy
        if(Input.GetKey(KeyCode.Keypad3)&&canEmotes==true)
        {
            Emote_Shy.SetActive(true);
            canEmotes=false;
        }
        if(Emote_Shy.activeInHierarchy==true){
            TimeUpdate();
           
        }

        //////////////////////////////////////////////
        if(waitTime==0)
            {
                endemotes();           
                
            }
      
       

        
    }
    private void endemotes()
    {
        Emote_Angly.SetActive(false);
        Emote_Happy.SetActive(false);
        Emote_Creepy.SetActive(false);
        Emote_Shy.SetActive(false);
        canEmotes=true;
        waitTime=5;
    }
    private void TimeUpdate()
    {
        if(waitTime>=0){
            waitTime-=Time.deltaTime;
        }
        else waitTime=0;
        
    }
}
