using System;
using UnityEngine;

public class BasicCharacterPreview : MonoBehaviour {
  #region Constants

  private static readonly int WALK_PROPERTY = Animator.StringToHash ("Walk");

  #endregion

  #region Inspector

  [SerializeField]
  private float speed = 2f;

  [Header ("Relations")]
  [SerializeField]
  private Animator animator = null;

  [SerializeField]
  private Rigidbody physicsBody = null;

  [SerializeField]
  private SpriteRenderer spriteRenderer = null;

  #endregion

  #region Fields

  private Vector3 _movement;

  #endregion

  #region MonoBehaviour

  private void Update () {
    // Vertical
    float inputY = 0;
    float inputX = 0;
    
    if (Input.GetKey (KeyCode.UpArrow)){
      inputY = 1;
      
      Debug.Log("PlayerGoUp");
    }

    else if (Input.GetKey (KeyCode.DownArrow)){
      inputY = -1;
      
      Debug.Log("PlayerGoDown");
    }
    
    // Horizontal
      if (Input.GetKey (KeyCode.RightArrow)) {
       inputX = 1;
      this.gameObject.transform.localScale = new Vector3 (1, 1, 1);
      
      Debug.Log("PlayerGoRight");

    } else if (Input.GetKey (KeyCode.LeftArrow)) {
      inputX = -1;
      this.gameObject.transform.localScale = new Vector3 (-1, 1, 1);
      
      Debug.Log("PlayerGoLeft");
    } 
     
     if(Input.GetKey (KeyCode.UpArrow)||Input.GetKey (KeyCode.DownArrow)||Input.GetKey(KeyCode.LeftArrow)||Input.GetKey(KeyCode.RightArrow))
     {
       animator.SetBool("IsWalking",true);
     }
     else animator.SetBool("IsWalking",false);

      //Running
     if(Input.GetKey(KeyCode.LeftShift)&&Input.GetKey (KeyCode.RightArrow))
    {
      animator.SetBool("IsRunning",true);
      animator.SetBool("IsWalking",false);
      speed=10f;
      Debug.Log("PlayerIsRunningRight");
    }
    else if(Input.GetKey(KeyCode.LeftShift)&&Input.GetKey (KeyCode.LeftArrow))
    {
      animator.SetBool("IsRunning",true);
      animator.SetBool("IsWalking",false);
      Debug.Log("PlayerIsRunningLeft");
    }
    else animator.SetBool("IsRunning",false);
         speed=6f;
     

    
    

    


    
    
    
    


    // Normalize
    _movement = new Vector3 (inputX, 0, inputY).normalized;

    /*animator.SetBool (WALK_PROPERTY,
      Math.Abs (_movement.sqrMagnitude) > Mathf.Epsilon);*/
  }

  private void FixedUpdate () {
    physicsBody.velocity = _movement * speed;
  }

  #endregion
}