using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class EmoteController : MonoBehaviourPun {

    [SerializeField] GameObject Emote_Angly;
    private float waitTime = 5;
    private bool canEmotes = true;

    private void Update () {
        if (photonView.IsMine) {

            //Angly

            if (Input.GetKey (KeyCode.Keypad0) && canEmotes == true) {

                photonView.RPC ("showemoteAngly", RpcTarget.All);

                canEmotes = false;

            }
            if (Emote_Angly.activeInHierarchy == true) {
                TimeUpdate ();

            }
            //Cooldown time
            if (waitTime == 0) {
                photonView.RPC ("endemote", RpcTarget.All);

            }
        }
    }

    [PunRPC]
    public void showemoteAngly () {
        Emote_Angly.SetActive (true);
    }

    [PunRPC]
    public void endemote () { //Add emote for disable
        Emote_Angly.SetActive (false);
        canEmotes = true;
        waitTime = 5;
    }

    private void TimeUpdate () {
        if (waitTime >= 0) {
            waitTime -= Time.deltaTime;
        } else waitTime = 0;

    }

}